﻿using System;

namespace ConsoleApp1
{
    static internal class Menu
    {
        static public void Show( int shibaCount)
        {
            Console.WriteLine(@"
░░░░░░░░░▄░░░░░░░░░░░░░░▄░░░░
░░░░░░░░▌▒█░░░░░░░░░░░▄▀▒▌░░░
░░░░░░░░▌▒▒█░░░░░░░░▄▀▒▒▒▐░░░
░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐░░░
░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐░░░
░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌░░░ 
░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒▌░░
░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐░░
░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄▌░
░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▌░
▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒▐░
▐▒▒▐▀▐▀▒░▄▄▒▄▒▒▒▒▒▒░▒░▒░▒▒▒▒▌
▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒▒▒░▒░▒░▒▒▐░
░▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒░▒░▒░▒░▒▒▒▌░
░▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▒▄▒▒▐░░
░░▀▄▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▄▒▒▒▒▌░░
░░░░▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀░░░
░░░░░░▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄▀░░░░░
░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▀▀░░░░░░░░");

            Console.WriteLine($"\nYou have {shibaCount} Shibas now!");

            Console.WriteLine($@"What do you want to do:
1. Get a Shibe!
2. Let your Shibes bork!
3. Feed your shibes
4. Walk your Shibes
5. Get the Happness of your shibe
6. Exit");
        }

        internal static void Init()
        {
            int menuRet = 0;
            Shibas shibas = new Shibas();

            while (menuRet != 6)
            {

                Menu.Show(shibas.Length);
                menuRet = SaveConvert(Console.ReadLine());


                switch (menuRet)
                {
                    case 1:
                        Console.WriteLine("1. Normal or 2. THICC?");
                        try
                        {
                            switch (SaveConvert(Console.ReadLine()))
                            {
                                case 1:
                                    shibas.add();
                                    break;
                                case 2:
                                    shibas.add();
                                    break;
                            }
                        }
                        catch (IndexOutOfRangeException e)
                        {
                            Console.WriteLine("You already own 10 Shibes!");
                        }
                        break;
                    case 2:
                        if(shibas.borkDel != null)
                            shibas.borkDel();
                        break;
                    case 3:
                        int foodRet = 0;
                        while (foodRet != 5)
                        {

                            Console.WriteLine(@"What do you want to feed your shibes:
    1. Bepis
    2. Spront
    3. Chimken Noggers
    4. Bread
    5. Nothing");
                            foodRet = SaveConvert(Console.ReadLine());
                            switch (foodRet)
                            {
                                case 1:
                                    shibas.feedDel(Food.Bepis);
                                    break;
                                case 2:
                                    shibas.feedDel(Food.Spront);
                                    break;
                                case 3:
                                    shibas.feedDel(Food.Chimken_Noggers);
                                    break;
                                case 4:
                                    shibas.feedDel(Food.Bread);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case 4:
                        string walkText = "";

                        foreach (Shiba shiba in shibas)
                        {
                            try
                            {
                                shiba.walkDel();
                            }
                            catch (ShibeNotWalkingException e)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine($"Walking Exception: {e.ErrorMessage}");
                            }
                        }

                        Console.ResetColor();
                        break;
                    case 5:
                        shibas.getHappnessDel();
                        break;

                    default:
                        break;
                }
                Console.ReadLine();
                Console.Clear();
            }
        }

        private static int SaveConvert(string v)
        {
            try
            {
                return Convert.ToInt32(v);
            }
            catch
            {
                return -1;
            }
        }
    }
}