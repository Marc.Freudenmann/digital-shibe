﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    struct Delegates
    {
        public delegate string borkDelegate();
        public delegate string feedDelegate(params object[] food);
        public delegate int getHappnessDelegate();
        public delegate string walkDelegate();
    }
}
