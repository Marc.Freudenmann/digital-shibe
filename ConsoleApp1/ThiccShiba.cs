﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ConsoleApp1.Delegates;

namespace ConsoleApp1
{
    class ThiccShiba : Shiba
    {
        private int ShibeNumber;

        public ThiccShiba(int number) : base(number)
        {
            ShibeNumber = number;
        }
        
        public override walkDelegate walkDel { get => walk; set => base.walkDel = value; }
        private string walk()
        {
            Random random = new Random();
            Thread.Sleep(10);
            random.Next(1, 6);

            if (random.Next(1, 13) == 1)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Oof let's go Walk");
                Console.ResetColor();
                return "Oof Let's go Walk";
            }
            else
            {
                throw new ShibeNotWalkingException(this, ShibeNumber);
            }
        }
    }
}
