﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    
    class ShibeNotWalkingException: SystemException
    {
        public string ErrorMessage = "Me no Walk today!";
        public ShibeNotWalkingException(Shiba shiba, int ShibeNumber)
        {
            ErrorMessage = $"({ShibeNumber}):";
            if (shiba is ThiccShiba)
                ErrorMessage += " Am too thicc";
        }
    }
}
