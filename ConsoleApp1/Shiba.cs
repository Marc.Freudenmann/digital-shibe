﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using static ConsoleApp1.Delegates;
using static ConsoleApp1.ShibaEventArgs;

namespace ConsoleApp1
{
    class Shiba: IShibaInterface
    {
        

        public borkDelegate borkDel;
        public feedDelegate feedDel;
        public getHappnessDelegate getHappnessDel;
        public virtual walkDelegate walkDel { get; set; }
        public EventHandler evt;

        private int ShibeNumber;
        private int happness = 50;

        public Shiba(int number)
        {
            ShibeNumber = number;
            borkDel = bork;
            feedDel = feed;
            getHappnessDel = getHappness;
            walkDel = walk;

            shibeNeedsAttention();

        }
        private string bork()
        {
            if(new Random().Next(1, 3)<2)
                evt(this, new ShibaEventArgs(ShibeNumber, ShibaEvent.Bork));
            if (new Random().Next(1, 3) < 2)
                evt(this, new ShibaEventArgs(ShibeNumber, ShibaEvent.Bork));
            if (new Random().Next(1, 3) < 2)
                evt(this, new ShibaEventArgs(ShibeNumber, ShibaEvent.Bork));
           
            return "Bork Bork Bork";
        }

        private string feed(params object[] foods)
        {
            string message = "";
            foreach (Food food in foods)
            {
                switch (food)
                {
                    case Food.Chimken_Noggers:
                        happness += 20;
                        message = $"Eating Mlem Mlem Mlem, Chimken Noggers!";
                        break;
                    case Food.Bepis:
                        happness += 10;
                        message = $"Slorp Slorp, Bepis best!";
                        break;
                    case Food.Spront:
                        happness += 10;
                        message = $"Slorp Slorp, Spront is verry gud!";
                        break;
                    case Food.Bread:
                        happness += 10;
                        message = $"Mlem Mlem Mlem, my favolite type of blead is Baguette!";
                        break;
                    default:
                        happness -= 20;
                        message = $"Am Hungeryboye!";
                        break;
                }
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Feeding ({ShibeNumber}): {message}");
                Console.ResetColor();
            }

            return message;

        }

        private int getHappness()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"Happness ({ShibeNumber}): {happness}");
            Console.ResetColor();
            return happness;
        }

        private string walk()
        {
            Random random = new Random();
            Thread.Sleep(10);
            random.Next(1, 3);

            if (random.Next(1, 3) == 1)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Walking ({ShibeNumber}): Let's go Walk");
                Console.ResetColor();
                return "Let's go Walk";
            }
            else
            {
                throw new ShibeNotWalkingException(this, ShibeNumber);
            }
        }

        private async void shibeNeedsAttention()
        {
            await Task.Run(() => shibeNeedsAttentionAsync());
        }
        private async void shibeNeedsAttentionAsync()
        {
            ShibaEventArgs args = new ShibaEventArgs(ShibeNumber, ShibaEvent.NeedAttention);
            evt = EventSubscriber.EShibaNeedaAttentionEvent;
            evt(this, args);
            happness += 20;
            borkDel();
            Thread.Sleep(new Random().Next(5000, 10000));
            await Task.Run(() => shibeNeedsAttentionAsync());
        }
    }
}
