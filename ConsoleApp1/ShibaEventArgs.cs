﻿using System;

namespace ConsoleApp1
{
    internal class ShibaEventArgs : EventArgs
    {
        public enum ShibaEvent
        {
            Bork,
            NeedAttention
        }
        public int shibeNumber;
        public ShibaEvent eventType;

        public ShibaEventArgs(int shibeNumber, ShibaEvent eventType)
        {
            this.shibeNumber = shibeNumber;
            this.eventType = eventType;
        }
    }
}