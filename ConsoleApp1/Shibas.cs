﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ConsoleApp1.Delegates;
using static ConsoleApp1.Shiba;

namespace ConsoleApp1
{
    class Shibas : IEnumerable
    {
        public int Length = 0;
        public borkDelegate borkDel;

        public feedDelegate feedDel;

        public getHappnessDelegate getHappnessDel;

        public virtual walkDelegate walkDel { get; set; }

        private Shiba[] shibaArray = new Shiba[10];
        IEnumerator IEnumerable.GetEnumerator()
        {
            for(int i = 0; i<Length; i++)
            {
                yield return shibaArray[i];
            }
        }

        internal void add()
        {
            Shiba shiba = new Shiba(Length);
            borkDel += shiba.borkDel;
            feedDel += shiba.feedDel;
            getHappnessDel += shiba.getHappnessDel;
            walkDel += shiba.walkDel;
            shibaArray[Length] = shiba;
            Length += 1;
        }
    }
}
