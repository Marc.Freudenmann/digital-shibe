﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

    class EventSubscriber:IDisposable
    {
        bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~EventSubscriber()
        {
            MessageBox((IntPtr)0, "Finalizer called", $"Disposing", 0);
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(!_disposed)
            {
                return;
            }

            if (disposing)
            {
                MessageBox((IntPtr)0, "Disposing is processed", $"Disposing", 0);
            }

            _disposed = true;
        }

        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        private static extern int MessageBox(IntPtr h, string m, string c, int type);

        public static void EShibaNeedaAttentionEvent(object sender,EventArgs args)
        {
            ShibaEventArgs shibeArgs = (ShibaEventArgs)args;
            if(shibeArgs.eventType == ShibaEventArgs.ShibaEvent.NeedAttention)
                MessageBox((IntPtr)0, "Press Ok to pet", $"Shibe {shibeArgs.shibeNumber} needs your attention! he is not happ :(", 0);
            else if (shibeArgs.eventType == ShibaEventArgs.ShibaEvent.Bork)
                MessageBox((IntPtr)0, "Bork", $"Borking ({shibeArgs.shibeNumber})", 0);

        }

    }
}
